import pandas as pd



def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def get_filled():
    df = get_titatic_dataframe()

    # Extract titles from the 'Name' column with a dot at the end
    df['Title'] = df['Name'].str.extract(' ([A-Za-z]+.)')

    titles = ["Mr.", "Mrs.", "Miss."]
    missing_counts = {}
    median_age_by_title = {}

    for title in titles:
        title_filter = df['Title'] == title
        missing_age_filter = title_filter & df['Age'].isna()
        
        missing_count = missing_age_filter.sum()
        median_age = df[title_filter]['Age'].median()
        
        missing_counts[title] = missing_count
        median_age_by_title[title] = int(median_age)
        df.loc[missing_age_filter, 'Age'] = median_age
    result = [[title, missing_counts[title], median_age_by_title[title]] for title in titles]
    return result
